import { Component, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  @Input() title!: String;

  constructor() { }

  ngOnInit(): void {
  }
  public alertar(){
    alert('Este app foi desenvolvido por Edson Matheus <3')
  }
}
