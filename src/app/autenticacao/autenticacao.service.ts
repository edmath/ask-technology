import { authMock } from './../mock/autenticationMock';
import { environment } from './../../environments/environment';
import { User } from './../shared/models/User';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AutenticacaoService {

  userToAutenticate : User = authMock.AUTENTICATION;

  constructor() { }

  public autenticar(username: String, password: String) : boolean {
    if(username == this.userToAutenticate.username && password == this.userToAutenticate.password){
      alert('Autenticado com sucesso.');
      return true;
    } else{
      alert('Login ou Senha inválidos!');
      return false;
    }
  }
}
