import { Itens } from './item';
export interface Pedido {
  id: String;
  status: String,
  previsaoEntrega: String;
  valorTotal: Number;
  totalDesconto: Number;
  endereco: String;
  formaPagamento: String;
  ocr: String;

  itens: Itens;

}

export type Pedidos = Array<Pedido>;
