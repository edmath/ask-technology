
export interface Item {

  id: String;
  descricao: String;
  valor: Number;
  quantidade: Number

}

export type Itens = Array<Item>;
