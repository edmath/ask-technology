export const listMock = {
  PEDIDOS : [

     {
      id: '9CV257',
      status: 'RECEBIDO',
      previsaoEntrega: '6 de Agosto de 2021',
      valorTotal: 1280.56,
      totalDesconto: 58.15,
      endereco: 'QUARTO ANEL VIÀRIO, 2165 - FORTALEZA/CE',
      formaPagamento: 'BOLETO - 14 DIAS',
      ocr: '83782',
      itens:[
        {
          id: '182',
          descricao: 'Placa de Vídeo NVIDIA',
          valor: 869.90,
          quantidade:1
        },
        {
          id: '332',
          descricao: 'Monitor 240hz',
          valor: 400.00,
          quantidade:1
        }
      ]
    },
    {
      id: '84CA45T',
      status: 'APROVADO',
      previsaoEntrega: '9 de Setembro de 2021',
      valorTotal: 495.80,
      totalDesconto: 15.50,
      endereco: 'SÂO JOSÈ, VILA 2165 - BRASILIA/DF',
      formaPagamento: 'DEBITO - A VISTA',
      ocr: '39872',
      itens:[
        {
          id: '344',
          descricao: 'Mobilete do Bem 10',
          valor: 1869.90,
          quantidade:5
        },
        {
          id: '516',
          descricao: 'Biela do Rotator',
          valor: 400.00,
          quantidade:2
        },
        {
          id: '330',
          descricao: 'Abajour 200amp',
          valor: 99.99,
          quantidade:1
        }
      ]
    },
    {
      id: '14OT05T',
      status: 'SEPARADO',
      previsaoEntrega: '8 de Junho de 2021',
      valorTotal: 220.80,
      totalDesconto: 7.50,
      endereco: 'JARDIM DOS PALMARES, GRAJAU - RIO DE JANEIRO/RJ',
      formaPagamento: 'CREDITO - 12 PARCELAS',
      ocr: '99726',
      itens:[
        {
          id: '885',
          descricao: 'MacBook Pro',
          valor: 12000.80,
          quantidade:1
        },
        {
          id: '572',
          descricao: 'Licença Windows',
          valor: 400.00,
          quantidade:20
        },
        {
          id: '872',
          descricao: 'Amplificador R280C MAX',
          valor: 2870,
          quantidade:2
        },
        {
          id: '091',
          descricao: 'SoftEngine RK9',
          valor: 900.00,
          quantidade:7
        },
        {
          id: '221',
          descricao: 'Vinho do Porto',
          valor: 800.00,
          quantidade:13
        }
      ]
    },
    {
      id: '99702M',
      status: 'EM_TRANSPORTE',
      previsaoEntrega: '1 de Janeiro de 2022',
      valorTotal: 3797.98,
      totalDesconto: 339.90,
      endereco: 'TANCREDO NEVES, BAIRRO FX DE GASA - REALENGO/RJ',
      formaPagamento: 'CARNE - 6 PARCELAS',
      ocr: '83782',
      itens:[
        {
          id: '013',
          descricao: 'Mustang GT',
          valor: 80000.00,
          quantidade:1
        },
        {
          id: '089',
          descricao: 'Camaro Amarelo',
          valor: 40000.00,
          quantidade:1
        },
        {
          id: '995',
          descricao: 'Apple',
          valor: 1.99,
          quantidade:20
        },
        {
          id: '133',
          descricao: 'AirMack Pro S',
          valor: 33333.90,
          quantidade:1
        }
      ]
    },
    {
      id: 'PP234M',
      status: 'ENTREGUE',
      previsaoEntrega: '6 de Agosto de 2021',
      valorTotal: 1280.56,
      totalDesconto: 58.15,
      endereco: 'QUARTO ANEL VIÀRIO, 2165 - FORTALEZA/CE',
      formaPagamento: 'BOLETO - 14 DIAS',
      ocr: '83782',
      itens:[
        {
          id: 'AS9842A032',
          descricao: 'Placa de Vídeo NVIDIA',
          valor: 869.90,
          quantidade:1
        },
        {
          id: '12397H13KK',
          descricao: 'Monitor 240hz',
          valor: 400.00,
          quantidade:1
        }
      ]
    }
]
};

