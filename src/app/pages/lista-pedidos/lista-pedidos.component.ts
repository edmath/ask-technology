import { Pedidos } from '../../models/pedido';
import { ListaPedidosService } from './lista-pedidos.service';
import { Component, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-lista-pedidos',
  templateUrl: './lista-pedidos.component.html',
  styleUrls: ['./lista-pedidos.component.scss']
})
export class ListaPedidosComponent implements OnInit {

  title : string = 'Pedidos';
  listaPedidos : Pedidos = [];

  constructor(
    private listaPedidosService : ListaPedidosService
  ) { }

  ngOnInit(): void {
    this.listarPedidos();
  }

  public listarPedidos(): Pedidos{
    this.listaPedidos = this.listaPedidosService.listarPedidos();
    return this.listaPedidos;
  }
}
