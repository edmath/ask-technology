import { listMock } from './../../mock/listMock';
import { Injectable } from '@angular/core';
import { Pedidos } from '../../models/pedido';

@Injectable({
  providedIn: 'root'
})
export class ListaPedidosService {

  listaPedidos : Pedidos = listMock.PEDIDOS;

  constructor() { }

  listarPedidos(): Pedidos{
    return this.listaPedidos;
  }
}
