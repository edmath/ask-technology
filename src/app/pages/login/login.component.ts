import { User } from './../../shared/models/User';
import { AutenticacaoService } from './../../autenticacao/autenticacao.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ErrorStateMatcher } from '@angular/material/core';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {

  emailFormControl = new FormControl('', [Validators.required, Validators.email]);
  senhaFormControl = new FormControl('', [Validators.required]);
  matcher = new MyErrorStateMatcher();

  user : User = {
  username : '',
  password : ''
  }

  constructor(
    private autenticacaoService : AutenticacaoService,
    private router : Router
  ) { }

  ngOnInit(): void {
  }

  login(){
    if (this.autenticacaoService.autenticar(this.user.username,this.user.password)) {
      this.router.navigate(['pages/pedidos']);
      console.log(this.user.username,this.user.password);
    }
  }

}
