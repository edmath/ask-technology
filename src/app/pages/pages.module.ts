import { SharedModule } from './../shared/shared.module';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { LoginComponent } from './login/login.component';

import {MatInputModule} from '@angular/material/input';
import {MatCardModule} from '@angular/material/card';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListaPedidosComponent } from './lista-pedidos/lista-pedidos.component';
import {MatListModule} from '@angular/material/list';
import { DetalhesPedidoComponent } from './detalhes-pedido/detalhes-pedido.component';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatMenuModule} from '@angular/material/menu';
import { ItensPedidoComponent } from './detalhes-pedido/itens-pedido/itens-pedido.component';
import {MatTableModule} from '@angular/material/table';



@NgModule({
  declarations: [
    LoginComponent,
    ListaPedidosComponent,
    DetalhesPedidoComponent,
    ItensPedidoComponent
  ],
  imports: [
    CommonModule,
    PagesRoutingModule,
    MatIconModule,
    MatInputModule,
    MatCardModule,
    FormsModule,
    MatListModule,
    MatToolbarModule,
    MatButtonModule,
    MatFormFieldModule,
    MatMenuModule,
    MatTableModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule

  ]
})
export class PagesModule { }
