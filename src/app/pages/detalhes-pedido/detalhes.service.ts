import { Itens } from './../../models/item';
import { listMock } from './../../mock/listMock';
import { Pedidos } from '../../models/pedido';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DetalhesService {

  listaPedidos: Pedidos = listMock.PEDIDOS;

  constructor() { }

  public buscarPorId(id: any): any{
     return this.listaPedidos.find(propertie=> propertie.id == id);
  }

  public listarItensDoPedido(id : any): Itens{
    var pedido = this.buscarPorId(id);
    console.log(pedido.itens);
    return pedido.itens;
  }
}
