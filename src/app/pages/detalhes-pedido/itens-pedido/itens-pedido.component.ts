import { Itens } from './../../../models/item';
import { DetalhesService } from './../detalhes.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-itens-pedido',
  templateUrl: './itens-pedido.component.html',
  styleUrls: ['./itens-pedido.component.scss']
})
export class ItensPedidoComponent implements OnInit {

  displayedColumns: string[] = ['id', 'descricao', 'valor', 'quantidade'];
  title: String = 'Itens Pedido';
  listaItens!: Itens;

  constructor(
    private activatedRoute : ActivatedRoute,
    private detalheService: DetalhesService
    ) { }

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.paramMap.get('id');
    this.listaItens = this.detalheService.listarItensDoPedido(id);
    console.log('itens',this.listaItens);

  }


}
