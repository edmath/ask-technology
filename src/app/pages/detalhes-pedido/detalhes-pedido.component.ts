import { Itens } from './../../models/item';
import { ListaPedidosService } from './../lista-pedidos/lista-pedidos.service';
import { DetalhesService } from './detalhes.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Pedido } from '../../models/pedido';

@Component({
  selector: 'app-detalhes-pedido',
  templateUrl: './detalhes-pedido.component.html',
  styleUrls: ['./detalhes-pedido.component.scss']
})
export class DetalhesPedidoComponent implements OnInit {

  title : String = 'Detalhes do Pedido';

  pedido!: Pedido;
  itens!: Itens;

  constructor(
    private detalhesPedidoService: DetalhesService,
    private listaPedidoService: ListaPedidosService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.paramMap.get('id');

    console.log('id do pedido',id);
    this.itens = this.detalhesPedidoService.listarItensDoPedido(id);
    this.pedido = this.detalhesPedidoService.buscarPorId(id);
    console.log(this.detalhesPedidoService.buscarPorId(id));

  }

  public verItensDoPedidoAlerta(){
    alert('Ver itens do pedido.');
  }

  public verNotaFiscalAlerta(){
    alert('Ver NotaFiscal. Obs.: Você será redirecionado para home pois o componente de nota fiscal não foi implementado.');
  }
}
