import { ItensPedidoComponent } from './detalhes-pedido/itens-pedido/itens-pedido.component';
import { DetalhesPedidoComponent } from './detalhes-pedido/detalhes-pedido.component';
import { ListaPedidosComponent } from './lista-pedidos/lista-pedidos.component';
import { LoginComponent } from './../pages/login/login.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path:'',
    redirectTo:'login'
  },
  {
    path:'login',
    component:LoginComponent
  },
  {
    path:'pedidos',
    component:ListaPedidosComponent
  },
  {
    path:'detalhes/:id',
    component:DetalhesPedidoComponent
  },
  {
    path:'detalhes/itens/:id',
    component:ItensPedidoComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
