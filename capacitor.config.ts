import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.asktech.dev',
  appName: 'ask-tech',
  webDir: 'dist/ask-tech',
  bundledWebRuntime: false
};

export default config;
